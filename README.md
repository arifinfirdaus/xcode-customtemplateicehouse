# README #

This is some file templates to make iOS development faster for Ice House's standard coding style.

## Getting Started 

### Prerequisites

Xcode

### Installing

Import this files (not the folder) to :
```
~/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/
```
Use Finder's `cmd+Shift+Go`.

## How to Use

Create new files on Xcode, scroll to botttom, and you will see <b>Ice House</b> section with custom file templates. 


![](Image.png)


## Authors

* **Arifin Firdaus**