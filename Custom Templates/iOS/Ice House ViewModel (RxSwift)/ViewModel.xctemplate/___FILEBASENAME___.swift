//___FILEHEADER___

import Foundation
import RxSwift
import RxCocoa

final class ___FILEBASENAMEASIDENTIFIER___: ViewModel {

	private let mutableState = BehaviorRelay<State?>(value: nil)
	private let disposeBag = DisposeBag()
}

extension ___FILEBASENAMEASIDENTIFIER___: ViewModel {

	var state: Observable<State> {
		mutableState.asObservable().filterNil()
	}

	func onReceiveEvent(_ event: Event) {

	}

}